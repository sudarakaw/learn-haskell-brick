# Text User Interface with Haskell & Brick

Practice code based on the video [Building Terminal User Interfaces with Haskell](https://www.youtube.com/watch?v=qbDQdXfcaO8)
by [Tom Sydney Kerckhove](https://github.com/NorfairKing)

- [Source](https://github.com/NorfairKing/tui-base)
