{-# LANGUAGE OverloadedStrings #-}

module Tui
  ( tui
  )
where


import           Brick.AttrMap
import           Brick.Main
import           Brick.Types
import           Brick.Util
import           Brick.Widgets.Core
import           Brick.Widgets.Border

import           Control.Monad.IO.Class

import           Cursor.Simple.List.NonEmpty

import qualified Data.List.NonEmpty            as NE

import           Graphics.Vty.Attributes.Color
import           Graphics.Vty.Input.Events

import           System.Directory
import           System.Exit


-- MODEL


newtype TuiState =
  TuiState { getStatePath :: NonEmptyCursor FSEntity}
  deriving (Show, Eq)

type ResourceName = String

data FSEntity = File FilePath | Dir FilePath
  deriving (Show, Eq)


-- UPDATE


handleTuiEvent :: TuiState -> BrickEvent n e -> EventM n (Next TuiState)
handleTuiEvent s e = case e of
  VtyEvent vtye -> case vtye of
    EvKey (KChar 'q') [] -> halt s
    EvKey KDown       [] -> do
      let nec = getStatePath s

      case nonEmptyCursorSelectNext nec of
        Nothing   -> continue s
        Just nec' -> continue $ s { getStatePath = nec' }
    EvKey KUp [] -> do
      let nec = getStatePath s

      case nonEmptyCursorSelectPrev nec of
        Nothing   -> continue s
        Just nec' -> continue $ s { getStatePath = nec' }
    EvKey KEnter [] -> do
      let fse = nonEmptyCursorCurrent $ getStatePath s

      case fse of
        File _  -> continue s
        Dir  fp -> do
          liftIO $ setCurrentDirectory fp
          s' <- liftIO buildInitialState

          continue s'

    _ -> continue s
  _ -> continue s


-- VIEW


drawTui :: TuiState -> [Widget ResourceName]
drawTui ts =
  let nec = getStatePath ts
  in  [ border $ vBox $ mconcat
          [ map (drawPath False) $ reverse $ nonEmptyCursorPrev nec
          , [drawPath True $ nonEmptyCursorCurrent nec]
          , map (drawPath False) $ nonEmptyCursorNext nec
          ]
      ]

drawPath :: Bool -> FSEntity -> Widget ResourceName
drawPath selected fse =
  (if selected then forceAttr "selected" else id) $ case fse of
    File f -> withAttr "file" $ str f
    Dir  d -> withAttr "dir" $ str d


-- MAIN


tuiApp :: App TuiState e ResourceName
tuiApp = App
  { appDraw         = drawTui
  , appChooseCursor = showFirstCursor
  , appHandleEvent  = handleTuiEvent
  , appStartEvent   = return
  , appAttrMap      = const $ attrMap
                        mempty
                        [("selected", fg red), ("dir", fg blue), ("file", fg yellow)]
  }

buildInitialState :: IO TuiState
buildInitialState = do
  contents <- getCurrentDirectory >>= getDirectoryContents >>= mapM
    (\fp -> do
      exists <- doesFileExist fp

      return $ if exists then File fp else Dir fp
    )

  case NE.nonEmpty contents of
    Nothing -> die "Unable to get directory content."
    Just ne -> return TuiState { getStatePath = makeNonEmptyCursor ne }

tui :: IO ()
tui = do
  buildInitialState >>= defaultMain tuiApp >>= print
